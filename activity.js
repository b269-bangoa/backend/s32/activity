let http = require("http");

const port = 3000;

let courses = [{"name": "BSM"}];

http.createServer(function (request, response){

  if( request.url == "/" && request.method == "GET") {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Welcome to Booking System');
  };
  if( request.url == "/profile" && request.method == "GET") {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Welcome to your profile!');
  };

//COURSES
  if( request.url == "/courses" && request.method == "GET") {
    response.writeHead(200, {'Content-Type': 'application/json'});
    response.write(JSON.stringify(courses));
    response.end('Here\'s our courses available');
  };

  //ADD COURSE
  if (request.url == "/addcourse" && request.method == "POST"){

      /*let requestBody = "";
      request.on('data', function (data) {
        requestBody += data;
      });
      request.on('end', function() {
        requestBody = JSON.parse(requestBody);
        let newCourse = { "name": requestBody.name};
        courses.push(newCourse);
      });*/
        response.writeHead(200, {'Content-Type': 'application/json'});
        response.end('Add course to our resources');
      };

  //UPDATE COURSE
  if (request.url == "/updatecourse" && request.method == "PUT") {

    response.writeHead(200, {'Content-Type': 'application/json'});
    response.end('Update a course to our resources');
    };

  //ARCHIEVE COURSE
  if (request.url == "/archievecourse" && request.method == "DELETE") {
    response.writeHead(200, {'Content-Type': 'application/json'});
    response.end('Archieve courses to our resources');
    };

}).listen(port);

console.log(`Server running at localhost: ${port}`); 